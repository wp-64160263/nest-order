/* eslint-disable prettier/prettier */
import { OrderItem } from 'src/orders/entities/order-items';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column({ type: 'float' })
  price: number;

  @OneToMany(()=>OrderItem, (orderItem) =>orderItem.product)
  orderItems: OrderItem[];
}
