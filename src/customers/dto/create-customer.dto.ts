/* eslint-disable prettier/prettier */
import { IsNotEmpty, Length } from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(4, 16)
  name: string;

  @IsNotEmpty()
  @Length(1, 2)
  age: number;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  @IsNotEmpty()
  @Length(1)
  gender: string;
}
