class CreateOrdeItemDto {
  productId: number;
  amount: number;
}

export class CreateOrderDto {
  customerId: number;
  orderItems: CreateOrdeItemDto[];
}
